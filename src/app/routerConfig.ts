import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';


const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  //getting error so i am commenting for some time ..
  //{
  //  path: 'users',
  //  loadChildren: './app/users/users.module#UsersModule',
    
  //}
];
export default appRoutes;

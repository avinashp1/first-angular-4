import { Component, OnInit } from '@angular/core';
import { ContactUsService } from './contact-us.service';



@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  name: string;
  email: string;
  message: string;
  private contactus = [];
  constructor(private backend: ContactUsService) { }

  ngOnInit() {
  }

  processForm() {
    this.backend.post_contactus(this.name,this.email,this.message).subscribe((res: any[]) => {
      console.log(res["id"]);
    });
  }

  //test call get api from fake server..
  //processForm() {
  //  this.backend.get_contactus().subscribe((res: any[]) => {
  //    this.contactus = res;
  //    console.log(this.contactus);
  //  });
  //}
}

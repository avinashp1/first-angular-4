import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'

})
export class ContactUsService {
  apiUrl = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {

  }
  get_contactus() {
    return this.httpClient.get(this.apiUrl + '/contactus');
  }
  post_contactus(name,email,message) {
    return this.httpClient.post(this.apiUrl + '/contactus', { name: name, email: email, message: message });
  }
}
